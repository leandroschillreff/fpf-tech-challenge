# FPF Tech Challenge

## Tecnologias

Tecnologias utilizadas para desenvolver o projeto:

- [NodeJS](https://nodejs.org/en/) - v18.17.0
- [TypeScript](https://www.typescriptlang.org/)
- [NestJS](https://docs.nestjs.com/) - v10.0.0
- [PostgreSQL](https://www.postgresql.org/)
- [Docker](https://www.docker.com/)
- [Angular](https://angular.io/) - v16.1.0
- [Bootstrap](https://getbootstrap.com/)

---

## Execução

### Clone o repositótio

Abra o terminal e cole o codigo abaixo:

```shell
git clone https://gitlab.com/leandroschillreff/fpf-tech-challenge.git
```

### Abra a pasta raiz do projeto

```shell
cd fpf-tech-challenge
```

---

## Execução da API

- Para executar a API é necessário ter instalado o Docker Compose

### Abra a pasta da api

- Abra um  terminal na raiz do projeto e abra a pasta api

```shell
cd api
```

### Instale as dependências

- Para instalar todas as dependências da API, execute o comando abaixo

```shell
npm install
```

### Comandos do docker para usar

- Se estiver o `PostgreSQL` instalado em sua máquina, é preciso parar a execução do mesmo, para não dar conflito com a porta `5432``, na hora de executar a API

    ```shell
    sudo service postgresql stop
    ```

- Se o primeiro comando não funcionar, execute em modo de administrador, "sudo"

1. Iniciar todos os serviçoes definidos em docker-compose.yml

    ```shell
    docker-compose up
    ```

    ```shell
    sudo docker-compose up
    ```

    - Se não ocorreu nenhum problema, a API vai estar rodando no seu computador

2. Para parar os serviços definidos em docker-compose.yml

    ```shell
    docker-compose stop
    ```

    ```shell
    sudo docker-compose stop
    ```

3. Para reiniciar os serviços parados anteriormente

    ```shell
    docker-compose start
    ```

    ```shell
    sudo docker-compose start
    ```

4. Parar a execução e remover os containers

    ```shell
    docker-compose down
    ```

    ```shell
    sudo docker-compose down
    ```

---

## Execução da Aplicação

- Abra um segundo terminal na raiz do projeto e abra a pasta frontend

```shell
cd frontend
```

- Instale as dependências do projeto

```shell
npm install
```

- Para executar a aplicação

```shell
npm start
```

- Pronto agora só acessar o link que aparecer no seu terminal no navegador
