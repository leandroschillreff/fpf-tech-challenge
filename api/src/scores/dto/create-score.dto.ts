import { IsNumber, IsString } from 'class-validator';

export class CreateScoreDto {
  @IsString()
  playerName: string;
  @IsNumber()
  score: number;
}
