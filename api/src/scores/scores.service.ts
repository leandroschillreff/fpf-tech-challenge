import { Injectable } from '@nestjs/common';
import { CreateScoreDto } from './dto/create-score.dto';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class ScoresService {
  constructor(private prisma: PrismaService) {}
  async create(createScoreDto: CreateScoreDto) {
    const { ...data } = createScoreDto;
    const createdScore = await this.prisma.score.create({ data: { ...data } });
    return createdScore;
  }

  async findAll() {
    const scores = await this.prisma.score.findMany();
    return scores;
  }
}
