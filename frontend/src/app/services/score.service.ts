import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Player, Score } from '../interfaces/Player';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ScoreService {
  private apiUrl = 'http://localhost:3000/scores';
  constructor(private http: HttpClient) {}

  getScores(): Observable<Player[]> {
    return this.http.get<Player[]>(this.apiUrl);
  }

  createScore(score: Score): Observable<Score> {
    return this.http.post<Score>(this.apiUrl, score);
  }
}
