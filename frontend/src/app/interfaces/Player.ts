export interface Player {
  id: string;
  playerName: string;
  date: string;
  score: number;
}

export interface Score {
  playerName: string;
  score: number;
}
