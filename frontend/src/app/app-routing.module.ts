import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { RankingComponent } from './pages/ranking/ranking.component';
import { RulesComponent } from './pages/rules/rules.component';
import { StartGameComponent } from './pages/start-game/start-game.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'start-game', component: StartGameComponent },
  { path: 'rules', component: RulesComponent },
  { path: 'ranking', component: RankingComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
