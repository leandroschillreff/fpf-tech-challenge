import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { faPlay } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-insert-name',
  templateUrl: './insert-name.component.html',
  styleUrls: ['./insert-name.component.css'],
})
export class InsertNameComponent implements OnInit {
  @Output() onSubmit = new EventEmitter<string>();
  @Input() insertNameData: string = '';
  faPlay = faPlay;
  insertNameForm!: FormGroup;

  ngOnInit(): void {
    this.insertNameForm = new FormGroup({
      name: new FormControl(this.insertNameData, [Validators.required]),
    });
  }

  get name() {
    return this.insertNameForm.get('name')!;
  }

  submit() {
    if (this.insertNameForm.value.name === '') {
      return;
    }
    this.onSubmit.emit(String(this.insertNameForm.value.name));
  }
}
