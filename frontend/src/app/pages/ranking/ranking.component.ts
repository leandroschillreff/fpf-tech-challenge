import { Component, OnInit } from '@angular/core';
import { Player } from 'src/app/interfaces/Player';
import { ScoreService } from 'src/app/services/score.service';
import { faAward } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.css'],
})
export class RankingComponent implements OnInit {
  scores: Player[] = [];
  faAward = faAward;

  constructor(private scoreService: ScoreService) {}

  ngOnInit(): void {
    this.scoreService.getScores().subscribe((score) => {
      this.scores = score;

      this.scores.sort(function (a, b) {
        return a.score > b.score ? -1 : a.score > b.score ? 1 : 0;
      });

      this.scores.forEach((item) => {
        item.date = new Date(item.date).toLocaleString('pt-BR');
      });
    });
  }
}
