import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { faGhost, faUserNinja } from '@fortawesome/free-solid-svg-icons';
import { ScoreService } from '../../../app/services/score.service';
declare let $: any;

@Component({
  selector: 'app-start-game',
  templateUrl: './start-game.component.html',
  styleUrls: ['./start-game.component.css'],
})
export class StartGameComponent {
  faUserNinja = faUserNinja;
  faGhost = faGhost;

  lifePlayer: number = 100;
  lifeMonster: number = 100;
  amountTurns = 0;
  changeTurn: boolean = true;
  useSpecialAttack: number = 0;
  stunnedMonster: boolean = false;
  logs: { player: boolean; type: number; message: string }[] = [];
  score: number = 0;

  playerName: string = '';
  messageGame: string = '';
  mesageRoute: string = '';

  constructor(private scoreService: ScoreService, private router: Router) {}

  getPlayerName(playerName: string) {
    this.playerName = playerName;
  }

  calculateDamage(min: number, max: number) {
    return Math.floor(Math.random() * (max - min) + min);
  }

  callModal(message: string, route: string) {
    this.messageGame = message;
    this.mesageRoute = route;
    $('#staticBackdrop').modal('show');
  }

  async getScore() {
    if (this.lifeMonster <= 0) {
      this.lifeMonster = 0;
      this.score = Math.floor((this.lifePlayer * 1000) / this.amountTurns);
      this.scoreService
        .createScore({
          playerName: this.playerName,
          score: this.score,
        })
        .subscribe((score) => score);

      this.callModal(
        `Você ganhou, sua pontuação foi: ${this.score}`,
        '/ranking'
      );
    }
    if (this.lifePlayer <= 0) {
      this.lifePlayer = 0;

      this.callModal(`Você perdeu!`, '/');
    }
  }

  giveUp() {
    this.callModal(`Você desistiu :(`, '/start-game');
    this.lifeMonster = 100;
    this.lifePlayer = 100;
    this.amountTurns = 0;
    this.changeTurn = true;
    this.useSpecialAttack = 0;
    this.stunnedMonster = false;
    this.logs = [];
    this.score = 0;
  }

  basicAttack() {
    const basicAttackDamage = this.calculateDamage(5, 10);
    this.lifeMonster -= basicAttackDamage;
    this.amountTurns += 1;
    this.useSpecialAttack += 1;
    this.changeTurn = false;
    this.logs.push({
      player: true,
      type: 1,
      message: `Jogador usou "Ataque Básico" (-${basicAttackDamage} pontos de vida)`,
    });

    this.getScore();
  }

  monsterAttack() {
    if (this.lifeMonster <= 0) {
      return;
    }
    setTimeout(() => {
      if (this.stunnedMonster) {
        this.logs.push({
          player: false,
          type: 4,
          message: `Monstro ficou atordoado!`,
        });
        this.stunnedMonster = false;
      } else {
        let monsterAttackDamage = this.calculateDamage(6, 12);

        if (this.amountTurns % 3 === 0) {
          monsterAttackDamage = this.calculateDamage(8, 16);
          this.logs.push({
            player: false,
            type: 4,
            message: `Monstro usou "Ataque Especial (-${monsterAttackDamage} pontos de vida)`,
          });
        } else {
          this.logs.push({
            player: false,
            type: 4,
            message: `Monstro usou "Ataque Básico (-${monsterAttackDamage} pontos de vida)`,
          });
        }

        this.lifePlayer -= monsterAttackDamage;

        this.getScore();
      }
      this.changeTurn = true;
    }, 700);
  }

  specialAttack() {
    const specialAttackDamage = this.calculateDamage(10, 20);

    const stunned = this.calculateDamage(1, 11);
    if (stunned <= 5) {
      this.stunnedMonster = true;
    }

    this.lifeMonster -= specialAttackDamage;
    this.amountTurns += 1;
    this.useSpecialAttack = 0;
    this.changeTurn = false;

    this.logs.push({
      player: true,
      type: 2,
      message: `Jogador usou o "Ataque Especial" (-${specialAttackDamage} pontos de vida)`,
    });

    this.getScore();
  }

  heal() {
    const healAmount = this.calculateDamage(5, 15);
    this.lifePlayer += healAmount;
    if (this.lifePlayer > 100) {
      this.lifePlayer = 100;
    }
    this.amountTurns += 1;
    this.useSpecialAttack += 1;
    this.changeTurn = false;

    this.logs.push({
      player: true,
      type: 3,
      message: `Jogador usou "Curar" (+${healAmount} pontos de vida)`,
    });
  }
}
